<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Repositories\MovieRepository;
use Illuminate\Http\Request;

class MovieController extends Controller
{
        
    protected $movieRepo;

    public function __construct(MovieRepository $movie){
        $this->movieRepo = $movie;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->movieRepo->obtenerMovie());
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        
        return response()->json($this->movieRepo->crearMovie($data));
    }

    public function editMovie(Request $request){
        $data = json_decode($request->getContent(), true);
        
        return response()->json($this->movieRepo->actualizarMovie($data));
    }

    public function deleteMovie(Request $request){
        $data = json_decode($request->getContent(), true);
        if($this->movieRepo->eliminarMovie($data)){
            return response()->json(["status"=>"success"]);
        }else{
            return response()->json(["status"=>"error"]);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie_id)
    {
        echo "update".$movie_id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
