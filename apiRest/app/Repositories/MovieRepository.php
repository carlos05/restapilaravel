<?php
namespace App\Repositories;

use App\Movie;
class MovieRepository{

	
	public function crearMovie($data){

		$movie =  new Movie();
        $movie->name = $data['name'];
        $movie->description = $data['description'];
        $movie->genre = $data['genre'];
        $movie->year = $data['year'];
        $movie->duration = $data['duration'];
        $movie->save();
        return $movie;
	}

	public function obtenerMovie(){
		return Movie::get();
	}

	public function actualizarMovie($data){
		$movie =  Movie::find($data['id']);
        $movie->name = $data['name'];
        $movie->description = $data['description'];
        $movie->genre = $data['genre'];
        $movie->year = $data['year'];
        $movie->duration = $data['duration'];
        $movie->save();
        return $movie;
	}

	public function eliminarMovie($data){
		try{
			$movie =  Movie::find($data['id']);
			$movie->delete();
			return true;	
		}catch(Exception $e){
			return false;
		}	
		
	}
}


?>